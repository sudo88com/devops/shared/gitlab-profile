# sudo88com

Welcome to the `sudo88com/devops/shared` repository! This repository is dedicated to our shared DevOps practices, tools, and configurations.

We have [docs](https://gitlab.com/sudo88com/devops/shared/library/) site to surface at:

- [https://sudo88com.gitlab.io/devops/shared/library/bash/](https://sudo88com.gitlab.io/devops/shared/library/bash/)

## Overview

This repository serves as a central hub for our DevOps automation scripts, configuration files, and best practices. It includes various tools and resources designed to streamline our development and deployment processes.

## Getting Started

To start using the resources in this repository, please check out our [GitLab CI/CD Templates](https://gitlab.com/sudo88com/devops/shared/ci-template).

## Contact

For any questions or suggestions, please contact the maintainers at [antyung41@gmail.com](mailto:antyung41@gmail.com).
